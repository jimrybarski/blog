+++
title="DS9 Viewing Guide"
description=""
date=2012-08-26T12:00:00-06:00
[taxonomies]
tags=["star trek", "tv"]
+++
I loved Star Trek: Deep Space Nine, but it really takes a long time for it to come into its own, and there are a lot of terrible episodes, especially in the first few seasons. However, since DS9 has a long-term plot and since there are a lot of great episodes in every season, I wrote this guide so friends could know which episodes they could skip and which ones they really do need to see.  
  
<u>Classification key:</u>  
<span class="critical">C</span>&nbsp;Critical to the long-term plot or character development  
<span class="recommended">R</span>&nbsp;Highly recommended, but not critical to the long-term plot  
<span class="good">G</span>&nbsp;Good and worth watching but does not affect the long-term plot  
<span class="terrible">T</span>&nbsp;Terrible  
  
All other (unlisted) episodes are neither good nor bad, watch them if you're just really into the show.

#### World building episodes from Star Trek: The Next Generation

These episodes set the stage for DS9.

<span class="critical">C</span>&nbsp;S4E12 - The Wounded
Introduces the Cardassians, one of the main antagonists in DS9. Also features O’Brien and Worf, who become main characters in DS9. The actor that plays the Cardassian captain in this episode plays a different Cardassian in DS9, but character-wise they’re very similar.  
<span class="critical">C</span>&nbsp;S5E03 - Ensign Ro: Introduces the Bajorans, who basically had a multi-decade occupation/holocaust done to them by the Cardassians. Ensign Ro was supposed to appear on DS9 but the actress declined; the character Kira is essentially the same person.  
<span class="critical">C</span>&nbsp;S6E10/11 - Chain of Command: Allegory to 1984. One of the highest rated episodes of TNG.  
<span class="critical">C</span>&nbsp;S7E24 - Preemptive Strike: More good background, terrorism stuff.  

## Season 1

<span class="critical">C</span>&nbsp;01/02 - Emissary: First episode so mandatory. Some context: the reason Sisko doesn’t like Picard is because in an episode of TNG, Picard was kidnapped by cyborgs (the Borg), turned into a cyborg, and he became their leader and went on a killing spree (which killed Sisko’s wife) before ultimately being saved and reverted to a human.  
<span class="critical">C</span>&nbsp;03 - Past Prologue: Introduces Garak, my favorite character.  
<span class="good">G</span>&nbsp;08 - Dax: Character building for Jadzia, and they explain the whole Trill thing in detail. Not a great episode but the multi-life thing comes up a lot.  
<span class="terrible">T</span>&nbsp;10 - Move Along Home</span>: So awful, do not watch this even for ironic reasons  
<span class="critical">C</span>&nbsp;11 - The Nagus: The first Ferengi-heavy episode. Typically, these serve as comic relief when the show gets really dark, and they’re generally good episodes. Still I’m going to mark them as optional. But this one is a good one to watch just because so many of the main characters are Ferengi, and a lot of them defy their culture, so it’s good to know where they come from.  
<span class="critical">C</span>&nbsp;13 - Battle Lines: Nice episode about war and peace, important development with the Kai.  
<span class="terrible">T</span>&nbsp;14 - The Storyteller</span>: Someone on the Internet wrote an essay about why this is the worst episode of any Star Trek series. It was long. Needless to say, skip this one.  
<span class="good">G</span>&nbsp;15 - Progress: Eminent domain, former rebel realizes she is now the establishment. Kira character-building.  
<span class="critical">C</span>&nbsp;19 - Duet: Great episode about war criminals.  
<span class="critical">C</span>&nbsp;20 - In the Hands of the Prophets: Allegory for teaching evolution in schools, imposing cultural values onto others, terrorism, power struggles. Two important characters are introduced.  

## Season 2 

<span class="critical">C</span>&nbsp;01-03 - The Homecoming/The Circle/The Siege: An insurgent group engages in shenanigans.  
<span class="critical">C</span>&nbsp;05 - Cardassians: Good Garak episode  
<span class="terrible">T</span>&nbsp;06 - Melora  
<span class="good">G</span>&nbsp;07 - Rules of Acquisition: The Ferengi go to the Gamma quadrant to do stuff. Some important foreshadowing but no game-changers.  
<span class="good">G</span>&nbsp;13 - Armageddon Game: One-off about biological weapons and the power/danger of information.  
<span class="good">G</span>&nbsp;14 - Whispers: A nice one-off episode, pretty science-fictiony.  
<span class="good">G</span>&nbsp;16 - Shadowplay: Another nice one-off episode, but includes some more foreshadowing.  
<span class="critical">C</span>&nbsp;18 - Profit and Loss: Cardassian rebels, Garak and Quark.  
<span class="good">G</span>&nbsp;19 - Blood Oath: There’s a really good line in this episode that I think of often  
<span class="critical">C</span>&nbsp;20/21 - The Maquis: Rebels, war, secret plans, etc.  
<span class="recommended">R</span>&nbsp;22 - The Wire: Mostly about addiction, but also goes deep into Garak’s spy-related past.   
<span class="critical">C</span>&nbsp;24 - The Collaborator: Power struggle to become the next space pope, new information on the culprit behind a massacre that occurred during the occupation of Bajor  
<span class="good">G</span>&nbsp;25 - The Tribunal: O’Brien is accused by the Cardassians of a crime he didn’t commit. Nothing is as it seems.  
<span class="critical">C</span>&nbsp;26 - The Jem’Hadar: One of the defining episodes of the show. The war (effectively) begins.   

## Season 3

<span class="critical">C</span>&nbsp;01/2 - The Search: Major plot point, great action  
<span class="good">G</span>&nbsp;03 - The House of Quark: Quark’s ex-wife rolls into town, shenanigans ensue  
<span class="good">G</span>&nbsp;05 - Second Skin: Kira might be a secret Cardassian  
<span class="good">G</span>&nbsp;06 - The Abandoned: They find a baby Jem’Hadar  
<span class="good">G</span>&nbsp;07 - Civil Defense: They accidentally trigger a program intended to thwart slave uprisings from back when the station was a mining facility run by the Cardassians  
<span class="critical">C</span>&nbsp;09 - Defiant: Major plot point.  
<span class="good">G</span>&nbsp;11/12 - Past Tense: I’m not actually that into these episodes, but here is a quote from Wikipedia about it: “as this episode was finishing production an article appeared in the Los Angeles Times describing a proposal by the mayor to create fenced-in "havens" for the city's homeless, to make downtown Los Angeles more desirable for business. The cast and crew were shocked that this was essentially the same scenario that Past Tense warned might happen in three decades, but was now being seriously proposed in the present.”  
<span class="critical">C</span>&nbsp;13 - Life Support: Major plot point, nature of conscious experience  
<span class="critical">C</span>&nbsp;14 - Heart of Stone: I don’t like the main plot of this episode but it’s relatively important, though the B-story is great.  
<span class="good">G</span>&nbsp;15 - Destiny: Literal interpretations of religious texts can be problematic  
<span class="good">G</span>&nbsp;16 - Prophet Motive: Decent Ferengi episode, a total one-off  
<span class="good">G</span>&nbsp;17 - Visionary: Just a good episode about time travel  

### A few more episodes from Star Trek: The Next Generation

To better know the Klingons and Romulans, who will be playing a much larger role in the show soon, I highly recommend watching a few episodes from The Next Generation: 

<span class="good">G</span>&nbsp;S2E08 - A Matter of Honor: (the most quotable episode of TNG)  
<span class="good">G</span>&nbsp;S3E10 - The Defector   
<span class="good">G</span>&nbsp;S4E26/S5E1 - Redemption: To set this up, Worf’s father was falsely accused of helping the Romulans commit a massacre decades ago, which the Duras family actually orchestrated. If this were to become public knowledge however, it would lead to a civil war, so after learning the truth, Worf agreed to publicly accept that his father was to blame in order to save the Empire. He later stabbed the head of the Duras family to death, but he was the only person who could have restored Worf’s honor. So Worf is stuck in this weird limbo where all Klingons hate him and there’s nothing anyone can do.

## Season 3 (continued)

<span class="critical">C</span>&nbsp;20 - Improbable Cause: Major plot point, great ep  
<span class="critical">C</span>&nbsp;21 - The Die is Cast: Major plot point, great ep  
<span class="good">G</span>&nbsp;23 - Family Business: Women’s rights  
<span class="critical">C</span>&nbsp;24 - Shakaar: Space pope power struggles and rebels  
<span class="critical">C</span>&nbsp;26 - The Adversary: War episode  

## Season 4

<span class="critical">C</span>&nbsp;01/02 - The Way of the Warrior: New characters, major plot points  
<span class="critical">C</span>&nbsp;04 - Hippocratic Oath: Philosophy of giving medical care to the enemy during a war  
<span class="critical">C</span>&nbsp;05 - Indiscretion: They search for a missing ship  
<span class="critical">C</span>&nbsp;07 - Starship Down: Good ep  
<span class="recommended">R</span>&nbsp;08 - Little Green Men: They go back in time, fun one-off episode  
<span class="good">G</span>&nbsp;10 - Our Man Bashir: Holodeck shenanigans  
<span class="critical">C</span>&nbsp;11/12 - Homefront/Paradise Lost: The fragility of democracy during a time of war and suspicion  
<span class="critical">C</span>&nbsp;13 - Crossfire: Seriously I hate this entire subplot but it matters to the show  
<span class="critical">C</span>&nbsp;14 - Return to Grace: A little slow but important developments occur  
<span class="good">G</span>&nbsp;15 - Sons of Mogh: Morality of assisted suicide  
<span class="good">G</span>&nbsp;16 - Bar Association: Fun Ferengi episode about labor rights and unions  
<span class="good">G</span>&nbsp;17 - Accession: Faith, caste systems, etc. I don’t care for this kinda stuff  
<span class="recommended">R</span>&nbsp;18 - Rules of Engagement : Worf commits a war crime but you aren’t supposed to do those  
<span class="recommended">R</span>&nbsp;19 - Hard Time: Very science-fictiony story about a novel way of punishing prisoners  
<span class="critical">C</span>&nbsp;22 - For the Cause: Sisko’s girlfriend might be fooling around on him  
<span class="critical">C</span>&nbsp;23 - To the Death: Renegade Jem’Hadar  
<span class="good">G</span>&nbsp;24 - The Quickening : Bashir helps with a plague, allegory for the third world in some respects  
<span class="good">G</span>&nbsp;25 - Body Parts: Ferengi episode  
<span class="critical">C</span>&nbsp;26 - Broken Link: Shit gets real  

## Season 5

<span class="critical">C</span>&nbsp;01 - Apocalypse Rising  
<span class="good">G</span>&nbsp;02 - The Ship  
<span class="critical">C</span>&nbsp;03 - Looking for par’Mach in All the Wrong Places  
<span class="recommended">R</span>&nbsp;04 - Nor the Battle to the Strong: MASH in space  
<span class="recommended">R</span>&nbsp;05 - The Assignment: O’Brien can’t catch a break  
<span class="recommended">R</span>&nbsp;06 - Trials and Tribble-ations: Only recommended if you have seen the original Tribbles episode from original Star Trek  
<span class="recommended">R</span>&nbsp;09 - The Ascent: The Odd Couple in desolate wilderness  
<span class="critical">C</span>&nbsp;10 - The Rapture: Sisko has prophetic visions  
<span class="good">G</span>&nbsp;11 - The Darkness and the Light: Someone is murdering Kira’s friends  
<span class="critical">C</span>&nbsp;12 - The Begotten: Major development  
<span class="recommended">R</span>&nbsp;13 - For the Uniform: A little ham-fisted with the allegory, but Sisko goes full beast mode  
<span class="critical">C</span>&nbsp;14 - In Purgatory’s Shadow: One of my favorite episodes of the entire series, very important, do not read the Netflix synopsis, don’t look at the preview screen cap  
<span class="critical">C</span>&nbsp;15 - By Inferno’s Light: Major plot point, do not read the Netflix synopsis, don’t look at the preview screen cap  
<span class="critical">C</span>&nbsp;16 - Doctor Bashir, I Presume?: Not that important but a secret about Bashir is revealed that comes up repeatedly from here on out  
<span class="good">G</span>&nbsp;18 - Business as Usual: Quark gets involved with an arms dealer  
<span class="good">G</span>&nbsp;19 - Ties of Blood and Water: More war crimes deliberations, callback to S3E05 “Second Skin”  
<span class="good">G</span>&nbsp; 20 - Ferengi Love Songs: Good Ferengi ep, as all Ferengi eps are  
<span class="good">G</span>&nbsp; 21 - Soldiers of the Empire: Good Klingon episode  
<span class="good">G</span>&nbsp; 22 - Children of Time: Science fictiony episode  
<span class="good">G</span>&nbsp; 23 - Blaze of Glory: Stolen nukes  
<span class="good">G</span>&nbsp; 24 - Empok Nor: Spooky episode  
<span class="critical">C</span>&nbsp;25 - In the Cards: Important strategic things with a nice comedic relief subplot  
<span class="critical">C</span>&nbsp;26 - Call to Arms: Shit gets real, but for real this time  

## Season 6

<span class="critical">C</span>&nbsp;01 - A Time to Stand  
<span class="critical">C</span>&nbsp;02 - Rocks and Shoals: This is the most metal of all episodes. Two of my hypothetical cats are named after characters in this episode, an honor not frequently bestowed. Try to guess who!  
<span class="critical">C</span>&nbsp;03 - Sons and Daughters  
<span class="critical">C</span>&nbsp;04 - Behind the Lines  
<span class="critical">C</span>&nbsp;05 - Favor the Bold  
<span class="critical">C</span>&nbsp;06 - Sacrifice of Angels  
<span class="critical">C</span>&nbsp;07 - You Are Cordially Invited…: If you watch this episode and get married and still don’t have a Klingon-themed wedding then you are history’s greatest monster  
<span class="critical">C</span>&nbsp;09 - Statistical Probabilities: Flowers for Algernon in space  
<span class="critical">C</span>&nbsp;10 - The Magnificent Ferengi: Yes, a non-optional Ferengi episode! Very war-related  
<span class="critical">C</span>&nbsp;11 - Waltz  
<span class="recommended">R</span>&nbsp;12 - Who Mourns for Morn?  
<span class="critical">C</span>&nbsp;13 - Far Beyond the Stars: One of the highest-rated episodes by DS9 fans. I don’t think it’s the greatest but I do like it, and it’s pretty unusual. And you get to see the actors without prosthetics. There’s also a brief reference to it in season 7 that will be super confusing if you haven’t seen it  
<span class="recommended">R</span>&nbsp;14 - One Little Ship: Just a fun one-off episode that makes a really good point about a fatal flaw in a well-known children’s movie  
<span class="recommended">R</span>&nbsp;15 - Honor Among Thieves  
<span class="recommended">R</span>&nbsp;16 - Change of Heart  
<span class="recommended">R</span>&nbsp;17 - Wrongs Darker than Death or Night  
<span class="critical">C</span>&nbsp;18 - Inquisition: Major downstream plot points based on this episode  
<span class="critical">C</span>&nbsp;19 - In the Pale Moonlight: Major plot point, Garak goes full Garak. One of my favorite episodes. Perhaps my favorite of all.  
<span class="good">G</span>&nbsp;20 - His Way: I just don’t like anything about this subplot, but it does have large implications for several characters. And they introduce a holodeck program that will be featured repeatedly. Protip: you can skip the music in every episode, nothing important ever happens during a song  
<span class="good">G</span>&nbsp;21 - The Reckoning  
<span class="recommended">R</span>&nbsp;22 - Valiant: Hubris, ambition, elitism  
<span class="recommended">R</span>&nbsp;23 - Profit and Lace: Women’s rights  
<span class="good">G</span>&nbsp;25 - The Sound of Her Voice: Mild science fiction  
<span class="critical">C</span>&nbsp;26 - Tears of the Prophets: Important war episode  

## Season 7

<span class="critical">C</span>&nbsp;01 - Image in the Sand  
<span class="critical">C</span>&nbsp;02 - Shadows and Symbols  
<span class="critical">C</span>&nbsp;03 - Afterimage   
<span class="terrible">T</span>&nbsp;04 - Take Me Out to the Holosuite: So poorly written and executed, everyone involved with this episode should be bricked up inside a wall  
<span class="good">G</span>&nbsp;05 - Chrysalis  
<span class="critical">C</span>&nbsp;06 - Treachery, Faith and the Great River  
<span class="critical">C</span>&nbsp;07 - Once More Unto the Breach   
<span class="critical">C</span>&nbsp;08 - The Siege of AR-558  
<span class="critical">C</span>&nbsp;09 - Covenant  
<span class="critical">C</span>&nbsp;10 - It’s Only a Paper Moon  
<span class="good">G</span>&nbsp;11 - Prodigal Daughter: A few bad things happen to O’Brien, and he’s not even the focus of the story. Held by the writers to be the worst episode of the season, presumably because no one wants to mention episode 704 out loud for fear of what happens to those who do. I thought it was fine.  
<span class="good">G</span>&nbsp;14 - Chimera: Odo meets a douchebag Changeling  
<span class="good">G</span>&nbsp; 15 - Badda-Bing, Badda-Bang: Oceans Eleven in space  
<span class="critical">C</span>&nbsp;16-26: The last 10 episodes are effectively one continuous plotline, and they’re all great.

