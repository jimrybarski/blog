+++
title="What I wish I could find in a workflow manager"
description=""
date=2022-01-05T10:00:00-06:00
[taxonomies]
tags=["programming", "workflow"]
+++
I've been dissatisfied with every workflow manager that I've used (Nextflow, Snakemake, doit and bpipe) and I haven't been able to find one that checks all my unspoken boxes. Let's make them spoken!

### Detecting code changes should be table stakes

The entire motivation for using a workflow manager is that it helps you guarantee that inputs were correctly transformed into outputs. Outputs are invalid if either the inputs OR the source code changes, yet every workflow manager that I know of (except Snakemake) only considers changes to data as necessitating a re-run of the pipeline. I'm sure this is a hard problem to solve in general, but a heuristic approach to identifying source files is probably good enough, especially if the user can manually specify source directories/files in case it's not.  
<br>

An alternative approach would be to keep the pipeline under CI control and trigger a run each time a commit is made. But this doesn't resolve which data is stale and which isn't - so the entire pipeline has to be run from scratch, which can be super expensive. Other workarounds might exist, but now we're just re-implementing the same feature over and over again. This behavior really belongs in the library.  
<br>
So Snakemake it is, right?

### DSL configurations are an unnecessary limitation

There is actually a great motivation for having a non-Turing-complete configuration file: it makes reasoning about the dependency graph much easier. However, it comes at the cost of forcing each user to learn a new domain-specific language (which cannot be used anywhere else), and if any specific behavior is missing, the user can't implement it. I actually gave up on bpipe because the configuration syntax made it literally impossible to accomplish something I needed to do. If a configuration file is just a script, there's always an implcit escape hatch for the user where they can work around the limitations of the library. [doit](https://pydoit.org/), [airflow](https://airflow.apache.org/) and [luigi](https://luigi.readthedocs.io/en/stable/), among others, all have executable configurations, so this is clearly not impossible. We shouldn't degrade the user experience of everyone, every time, just to help a few people out when they accidentally put an infinite loop in their config.

### Output files should be immutable and preserved after each run of the pipeline

There are two problems I'd like to solve here:   
1. A pipeline run creates some output files, and a subsequent change to the code stops creating those files. The workflow manager has no way to know that it should delete these files, so they're just left there. Especially with a long-evolving project, there are lots of these and deleting files is scary when some of them were very expensive to create.
2. I often find myself wanting to compare two versions of a file to learn how a code change affected it between runs, but most (all?) workflow managers will just overwrite the old file.

One way to solve both of these issues is to have each run of the pipeline create a new directory for all output files, and it either symlinks files from previous runs if they don't require an update, or it otherwise just puts new files there. This way, each output directory can never contain any cruft, and you can "time travel" between each run of the entire history of the project. Obviously, you'd want some simple tools to coalesce all the symlinks into real files at the end of the project, or to delete old runs if storage is limited, but those are trivial to implement.   
<br> 
This almost sounds like git, but git can't solve the cruft problem unless you delete all output files between each run, but that's an even worse problem unless your pipeline can run super quickly. 

### Conclusions

Existing workflow managers are deeply complex for a reason: they have been resolving difficult bugs for years. Snakemake, for example, will update timestamps of files on a remote server whose clock is not in sync with the runner's clock, if it notices that they're older than the files they depend on for creation. Creating a new workflow manager implicitly re-introduces the bugs that such behavior solves, and especially with distributed computation, starting from scratch is a dangerous proposition. That said, I think there's definitely space for a local-only workflow manager that works the way I've described here.
