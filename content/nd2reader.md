+++
title="nd2reader"
description=""
date=2015-05-26T12:00:00-06:00
[taxonomies]
tags=["programming"]
+++
I recently published [nd2reader](https://github.com/jimrybarski/nd2reader), a Python library that reads .nd2 image files produced by [NIS Elements](https://www.nikoninstruments.com/Products/Software). Most of the credit goes to the authors of [SLOTH](https://pypi.python.org/pypi/SLOTH/) who figured out how nd2s are actually structured.

nd2reader improves on things in a few ways. It can associate metadata with individual images, so you can know the channel, timestamp, field of view and z-level of any particular image, which I believe is novel among all the nd2 reading libraries. It also provides a simple, easy-to-use interface and (most importantly to me) does not require Java alongside Python, as Bio-Formats does.

Since it's now in PyPI, you can install it with:

```pip install nd2reader```

Update: I handed over control of this project and no longer maintain it. 
