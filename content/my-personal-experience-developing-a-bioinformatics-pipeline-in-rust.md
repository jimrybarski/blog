+++
title="My personal experience developing a bioinformatics pipeline in Rust"
description=""
date="2023-01-06T12:37:40-06:00"
[taxonomies]
tags = ["rust,", "bioinformatics,", "science"]
+++
I recently finished developing a pipeline almost entirely in Rust (hoping to get the preprint out soon). Initially, I began writing it in Python until I realized that this would probably be the last chance for me to write Rust professionally for a while - most of the computational biology jobs I'm finding are making the (correct) decision to stick with Python or R for now. Anyway, here are my thoughts:

### Prior experience with Rust

I have been learning Rust in fits and starts since 2015, but the only meaningful thing I've shipped until now has been my [autogram generator](https://github.com/jimrybarski/autogram), which was just for fun. Still, that was enough to give me an intuition for the borrow checker and Rust idioms in general, so there wasn't much of a learning curve when developing the pipeline.

### Performance

I typically found a 5-8x speedup over Python, which isn't the spectacular gain I was hoping for. However, since I was building a brand new pipeline from scratch, iteration speed was actually pretty important, so the better way to think of an 8x speedup in this context is that it makes a 12-hour task go from something you can iterate on once a workday to five times a day. Parallelization is almost trivial with `rayon` and I'd much rather use that than GNU parallel or Python's `subprocessing` module. Overall, I felt more free to experiment with various approaches knowing that nothing would ever turn into an overnight job.

### Library ecosystem

For parsing/writing plain text data, basically everything is there and as performant as you could want. I've been impressed with [noodles](https://docs.rs/noodles/latest/noodles/) for HTS file formats and [gb-io](https://docs.rs/gb-io/latest/gb_io/) for Genbank files.   
<br>
Surprisingly, there's still no crate with a full suite of statistical tools. Even something like a t-test seems to only be implemented in sketchy toy projects. Visualization is a bit better - there is [plotters](https://docs.rs/plotters/latest/plotters/), but while it's really starting to come together, I didn't want to risk needing to be able to make a particular kind of plot only to find that it's not implemented yet. So I still have a handful of Python scripts for all my statistical analyses and figure generation.   
<br> 
I couldn't find a library in Python, R or Rust that could generate gene diagrams the way I needed (I was doing something a bit unconventional), so I wrote my own using the `svg` crate. I was able to make a slightly-imperfect yet fully-functional gene diagram generator in a day, which included the time it took to learn the `svg` crate and how SVGs work. Check it out: 
<br><br>
![gene diagram showing a CRISPR operon](/jjjenes-diagram.png)

My takeaway from all of this is that the Rust ecosystem is incredibly mature in some areas but not others, and it's really not obvious what to expect until you look.

### Ergonomics 

Being able to add `#[forbid(unused, undocumented)]` (which prevents you from compiling until you write documentation and eliminate unused code) almost instantly increased the quality of the library and was such a breath of fresh air. I did a direct port of a few Python scripts in the beginning, and this also exposed a bunch of unused code and unused parameters (these maybe could have been found by a Python linter, which I should have been using, but in general these can't make the same guarantees that cargo can).   
<br>
It would be so much easier to teach Python to biologists if the packaging system worked as well as cargo. Cross-platform compatibility (and no virtual environments!) should be table stakes for package managers. Other people have written about all the lovely aspects of Rust, cargo, and clippy, so I won't repeat them, but it really is a joy to work with, especially as the project becomes too large to reason about all at once.

### Conclusions

The decision to adopt Rust for life sciences research is definitely an organizational one, and it's just not able to completely replace existing languages yet due to the major library deficiencies I described above. But partially adopting it into a pipeline is absolutely doable and could lead to meaningful gains in performance, correctness and maintainability.
