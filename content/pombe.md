+++
title="Fission yeast don't age"
description=""
date=2017-01-31T12:00:00-06:00
[taxonomies]
tags=["science", "aging"]
+++
My [colleagues](http://www.finkelsteinlab.org) and I just published our work on fission yeast aging in [eLIFE](https://elifesciences.org/articles/20340). Although we had hoped to develop a more human-like single-celled model organism to study eukaryotic aging, we came to the surprising conclusion that fission yeast simply don't age. That is, the chance of a fission yeast dying is constant throughout its lifespan. Still, it's a fascinating result.
