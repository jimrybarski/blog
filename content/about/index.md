+++
title = "About"
description = "About"
date = 2012-01-01T12:00:00-06:00
[extra]
is_page = true
+++
I recently finished my biochemistry PhD in the [Finkelstein Lab](https://finkelsteinlab.org) at the University of Texas at Austin, where I focused on metagenomic bioprospecting and CRISPR specificity.

### Contact
Personal: [jim@rybarski.com](mailto:jim@rybarski.com)  
Work: [jimrybarski@utexas.edu](mailto:jimrybarski@utexas.edu)  
Twitter: [@jimrybarski](https://www.twitter.com/jimrybarski)  
Github: [@jimrybarski](https://github.com/jimrybarski/)  

### GPG
Here's my [public key](james-rybarski.asc)

### Credits

My favicon is from [Flaticon](https://www.flaticon.com/free-icon/dna_882976?related_id=882976)
