+++
title="Terminator the Second"
description=""
date=2014-07-31T12:00:00-06:00
[taxonomies]
tags=["tv"]
+++
> Terminator the Second is a parody of the film Terminator 2: Judgment Day, constructed solely of lines and phrases from the plays of William Shakespeare. Only proper nouns, pronouns and verb tenses are subject to change, enabling us to remain true to the words of Shakespeare in form, if less so in intent.   
   
![Terminator the Second](/110logo.jpg)

> Let this pernicious hour stand aye accursed in the calendar. Our ingenious instrument that we but teach bloody instructions, which being taught return to plague the inventor, razeth our cities and subverts our towns, and in a moment, makes them desolate. They call it: Skynet."
   
[Watch](https://www.youtube.com/watch?v=TcrePuu5QUg) it or [get the soundtrack here.](http://watch.terminatorthesecond.com/)
